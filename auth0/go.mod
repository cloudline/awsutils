module bitbucket.org/cloudline/awsutils/auth0

require (
	github.com/aws/aws-lambda-go v1.10.0
	github.com/aws/aws-sdk-go v1.19.14 // indirect
	github.com/aws/aws-xray-sdk-go v0.9.4
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dvsekhvalnov/jose2go v0.0.0-20180829124132-7f401d37b68a
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.3.0
)

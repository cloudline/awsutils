package auth0

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"net/http"
	"strings"
)

////////////////////////////////////////////////////////////////////////////////
// STRUCTURES

type JsonWebKeySet struct {
	Keys []jsonWebKey `json:"keys"`
}

type jsonWebKey struct {
	KeyType              string   `json:"kty"`
	PublicKeyUse         *string  `json:"use,omitempty"`
	KeyOperations        *string  `json:"key_ops,omitempty"`
	Algorithm            *string  `json:"alg,omitempty"`
	KeyID                *string  `json:"kid,omitempty"`
	X509URL              *string  `json:"x5u,omitempty"`
	X509CertificateChain []string `json:"x5c,omitempty"`
	X509ThumbprintSHA1   *string  `json:"x5t,omitempty"`
	X509ThumbprintSHA265 *string  `json:"x5t#S256,omitempty"`
}

////////////////////////////////////////////////////////////////////////////////
// PUBLIC FUNCTIONS

func LoadJSONWebKeySet(url string) (JsonWebKeySet, error) {
	keySet := JsonWebKeySet{}

	// Fetch the key set from the URL
	resp, err := http.Get(url)
	if err != nil {
		return keySet, err
	}
	defer resp.Body.Close()

	// Parse the response body

	err = json.NewDecoder(resp.Body).Decode(&keySet)
	if err != nil {
		return keySet, err
	}

	return keySet, nil
}

func (jwk *jsonWebKey) X509CertificateChainPEM() string {
	certCount := len(jwk.X509CertificateChain)
	lines := make([]string, 0, 10*certCount) // about 10 lines per cert
	for _, cert := range jwk.X509CertificateChain {
		canonicalCert := WrapBase64(cert)
		lines = append(lines, "-----BEGIN CERTIFICATE-----")
		lines = append(lines, canonicalCert...)
		lines = append(lines, "-----END CERTIFICATE-----")
	}

	pem := strings.Join(lines, "\r\n")
	return pem
}

func WrapBase64(b64Str string) []string {
	const maxLineBytes = 48 // 48 raw bytes => 64 base64 characters

	// Decode the base64 value to []byte

	var decoder *base64.Encoding
	if strings.HasSuffix(b64Str, "=") {
		decoder = base64.StdEncoding
	} else {
		decoder = base64.RawStdEncoding
	}

	decoded, err := decoder.DecodeString(b64Str)
	if err != nil {
		log.Printf("unable to parse base64 encoded value: %v", b64Str)
		return []string{b64Str}
	}

	decodedLen := len(decoded)

	// Build []string of the lines of the output

	encoding := base64.StdEncoding

	lineEstimate := 1 + (decodedLen / maxLineBytes)
	lines := make([]string, 0, lineEstimate)

	curIdx := 0
	for curIdx < decodedLen {
		var lineBytes []byte

		nextIdx := curIdx + maxLineBytes
		if nextIdx > decodedLen {
			// remainder of decoded bytes fit on one line
			lineBytes = decoded[curIdx:]
		} else {
			lineBytes = decoded[curIdx:nextIdx]
		}

		line := encoding.EncodeToString(lineBytes)
		lines = append(lines, line)

		// prepare to process next line
		curIdx = nextIdx
	}

	return lines
}

package auth0

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"strings"
)

// JWTToken is an object to store token information
// after parsing for validation or claim-access purposes
type JWTToken struct {
	SigningAlgorithm string
	Signature        []byte
	rawTokenBody     []byte
	Header           map[string]interface{}
	Claims           map[string]interface{}
}

// ParseAndValidate takes a token string as received from a Lambda
// event and turns it into a validated and ready to use JWTToken struct
func ParseAndValidate(tokenStr, jwkURL string) (*JWTToken, error) {

	// first extract the token from the complete authorization value
	token, err := extractBearerToken(tokenStr)
	if err != nil {
		errorMessage := "error extracting bearer token: " + err.Error()
		return nil, errors.New(errorMessage) // Return a 401 Unauthorized
	}

	// just do a simple parsing on the token string to get the key id
	j, err := Parse(token)
	if err != nil {
		errorMessage := "error doing pre-validation token parsing: " + err.Error()
		return nil, errors.New(errorMessage)
	}

	keyID := j.Header["kid"]
	if keyID == nil {
		errorMessage := "No key id found in pre-validated token"
		return nil, errors.New(errorMessage)
	}

	// now validate the signature
	isValid, err := validateTokenSignature(keyID.(string), token, jwkURL)
	if !isValid {
		errorMessage := "token did not validate successfully"
		return nil, errors.New(errorMessage)
	}

	// all is well, return a pointer to the validated token
	return j, nil
}

// Parse parses a token string and returns a pointer
// to a JWTToken and error. Please note that this function
// merely parses the token, validation must be done after
// a JWTToken is returned. Typically you want to use ParseAndValidate() to
// get both operations done at once.
func Parse(tokenStr string) (*JWTToken, error) {
	rawSegs := strings.Split(tokenStr, ".")
	token := JWTToken{}

	header, err := base64.RawURLEncoding.DecodeString(rawSegs[0])
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(header, &token.Header)
	if err != nil {
		return nil, err
	}

	if token.Header["typ"] != "JWT" {
		return nil, errors.New("failed to parse, not a jwt token")
	}

	if token.Header["alg"] != "RS256" {
		alg := ""
		if token.Header["alg"] != nil {
			alg = token.Header["alg"].(string)
		}
		return nil, errors.New("unsupported signing algorithm: " + alg)
	} else {
		token.SigningAlgorithm = "RS256"
	}

	claims, err := base64.RawURLEncoding.DecodeString(rawSegs[1])
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(claims, &token.Claims)
	if err != nil {
		return nil, err
	}

	sig, err := base64.RawURLEncoding.DecodeString(rawSegs[2])
	if err != nil {
		return nil, err
	}

	token.Signature = sig
	token.rawTokenBody = []byte(fmt.Sprintf("%s.%s", rawSegs[0], rawSegs[1]))

	return &token, nil
}

// Extract token according to https://tools.ietf.org/html/rfc6750#section-2.1
func extractBearerToken(authorizationHeader string) (string, error) {

	authorizationHeaderFields := strings.Split(authorizationHeader, " ")
	if len(authorizationHeaderFields) != 2 {
		return "", fmt.Errorf("invalid structure of header in '%s'", authorizationHeader)
	}
	if strings.ToLower(authorizationHeaderFields[0]) != "bearer" {
		return "", fmt.Errorf("unsupported authentication scheme in '%s'", authorizationHeader)
	}
	return authorizationHeaderFields[1], nil
}
func validateTokenSignature(keyID, token, jwkURL string) (bool, error) {

	// this is a function ref that is used to get the JWT keys available from the Auth0 endpoint
	// and turns the result into  PEM file so that the library can validate
	keyRetrievalFunc := func(token *jwt.Token) (interface{}, error) {
		// get a JSON web ket set from the Auth0 Tenant endpoint
		response, _ := LoadJSONWebKeySet(jwkURL)
		var pemRef string
		for _, jwk := range response.Keys {
			if jwk.KeyID != nil && *jwk.KeyID == keyID {
				pemRef = jwk.X509CertificateChainPEM()
				break // found the key - done here
			}
		}
		// make sure we got a pem
		if pemRef == "" {
			return false, errors.New("unable to locate public key from Auth0")
		}

		// now we convert the JWK into a PEM file format
		pem := []byte(pemRef)
		key, err := jwt.ParseRSAPublicKeyFromPEM(pem)
		if err != nil {
			return false, errors.New("error while attempting to convert jwk data into PEM format")
		}

		return key, err
	}

	// now use the inline function to get the key as a PEM file and do the actual validation
	// note that we dont care about the JWT object returned since we have already parsed the JWT token
	_, err := jwt.Parse(token, keyRetrievalFunc)
	if err != nil {
		fmt.Println("Error doing the validation: ", err.Error())
		return false, errors.New("signature validation failed")
	}

	// if we got here the validation was successful
	return true, nil
}

# Cloudline AWS Utilities

## Dynamodb package
We created a reusable library to interact with custom structs and DynamoDB. See docs [here](https://bitbucket.org/cloudline/awsutils/src/master/docs/http.md)

## HTTP Package
Simple helpers to interact with APIGateway Responses. See docs [here](https://bitbucket.org/cloudline/awsutils/src/master/docs/http.md)

## Auth0 Package
This package is designed to be used by AWS Lambda authorization handlers to authenticate users via Auth0.
Detailed documentation and examples here: [here](https://bitbucket.org/cloudline/awsutils/src/master/docs/auth0.md)

## Logging Package
This package implements the Cloudline Solutions standard logging pattern (see Confluence page for high-level concept).
Detailed documentation and examples here: [here](https://bitbucket.org/cloudline/awsutils/src/master/docs/logging.md)
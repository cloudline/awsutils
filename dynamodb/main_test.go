package dynamodb

import (
	"context"
	"testing"
	"time"

	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
)

type testVertical struct {
	ID         string `json:"ID"`
	RefreshKey string `json:"refreshKey"`
	Source     string `json:"source"`
}

type testEvent struct {
	CloudEventsVersion string      `json:"cloudEventsVersion"`
	ContentType        string      `json:"contentType"`
	CorrelationID      string      `json:"correlationID"`
	Data               interface{} `json:"data"`
	EventID            string      `json:"eventID"`
	EventType          string      `json:"eventType"`
	EventTime          string      `json:"eventTime"`
	RecordingID        string      `json:"recordingID"`
	SchemaURL          string      `json:"schemaURL"`
	Source             string      `json:"source"`
}

func TestGetItems(t *testing.T) {
	// arrange
	var message string
	var err error

	ctx := context.Background()
	ctx, _ = xray.BeginSegment(ctx, "TestGetItem")

	table := NewTable(ctx, "heb-verticals-table-dev", "us-east-1")

	t.Run("get a single item", func(t *testing.T) {
		// arrange
		dest := &testVertical{}
		input := &Input{Key: map[string]interface{}{"ID": "nJMYKemsVTaVfBnWYoiqC97AVhDG4MHj9REfQZyn"}}

		// act
		if err = table.GetItem(input, dest); err != nil {
			message = err.Error()
		}

		// assert
		assert.Nil(t, err, message)
		assert.NotEmpty(t, dest.ID)
	})

	t.Run("get from scan", func(t *testing.T) {
		// arrange
		input := &Input{
			Params: map[string]interface{}{"source": "nico-wernli"},
		}

		// act
		list, err := table.Scan(input)
		if err != nil {
			message = err.Error()
		}

		dest := make([]*testVertical, len(list.items))
		if err := list.UnmarshalList(&dest); err != nil {
			message = err.Error()
		}

		// assert
		assert.Nil(t, err, message)
		assert.NotEmpty(t, dest)
		for _, v := range dest {
			assert.Equal(t, "nico-wernli", v.Source)
		}
	})

	t.Run("get with MustScan", func(t *testing.T) {
		// arrange
		var message string
		var err error
		input := &Input{
			Params: map[string]interface{}{"source": "nico-wernli"},
		}

		// act
		dest := make([]*testVertical, 0)
		if err := table.MustScan(input).UnmarshalList(&dest); err != nil {
			message = err.Error()
		}

		// assert
		assert.Nil(t, err, message)
		assert.NotEmpty(t, dest)
		for _, v := range dest {
			assert.Equal(t, "nico-wernli", v.Source)
		}
	})
}

func TestHistoryTable(t *testing.T) {
	ctx := context.Background()
	ctx, _ = xray.BeginSegment(ctx, "TestHistoryTable")

	eventTable := NewTable(ctx, "heb-event-history-table-dev", "us-east-1")
	input := &Input{
		Filter: "#eventType = :eventType AND #eventTime >= :eventTime",
		Params: map[string]interface{}{
			"eventType": "some-event-type",
			"eventTime": "2019-03-06T14:00:00Z",
		},
	}

	t.Run("get with Query", func(t *testing.T) {
		var message string
		list, err := eventTable.Query("heb-eventType-timestamp-index", input)
		if err != nil {
			message = err.Error()
		}

		assert.Nil(t, err, message)
		assert.NotEmpty(t, list.Items())
	})

	t.Run("get with MustQuery", func(t *testing.T) {
		dest := make([]*testEvent, 0)
		err := eventTable.MustQuery("heb-eventType-timestamp-index", input).UnmarshalList(&dest)

		assert.Nil(t, err)
		assert.NotEmpty(t, dest)
	})
}

func TestPutItem(t *testing.T) {
	ctx := context.Background()
	ctx, _ = xray.BeginSegment(ctx, "TestPutItem")

	verticalsTable := NewTable(ctx, "heb-verticals-table-dev", "us-east-1")
	vertical := &testVertical{
		ID:         uuid.Must(uuid.NewV4()).String(),
		RefreshKey: uuid.Must(uuid.NewV4()).String(),
		Source:     uuid.Must(uuid.NewV4()).String(),
	}

	err := verticalsTable.PutItem(&Input{Item: vertical, Filter: "attribute_not_exists(id)"})

	assert.Nil(t, err)
}

func TestUpdateItem(t *testing.T) {
	// assert
	ctx := context.Background()
	ctx, _ = xray.BeginSegment(ctx, "TestUpdateItem")

	verticalsTable := NewTable(ctx, "heb-verticals-table-dev", "us-east-1")
	vertical := &testVertical{
		ID:         uuid.Must(uuid.NewV4()).String(),
		RefreshKey: uuid.Must(uuid.NewV4()).String(),
		Source:     uuid.Must(uuid.NewV4()).String(),
	}

	err := verticalsTable.PutItem(&Input{Item: vertical})
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(1)
	update := &Input{
		Params: map[string]interface{}{"source": "nwernli-source"},
		Key:    map[string]interface{}{"ID": vertical.ID},
	}

	// act
	err = verticalsTable.UpdateItem(update)
	time.Sleep(1)

	// assert
	assert.Nil(t, err)
}

func TestDeleteItem(t *testing.T) {
	ctx := context.Background()
	ctx, _ = xray.BeginSegment(ctx, "TestDeleteItem")

	verticalsTable := NewTable(ctx, "heb-verticals-table-dev", "us-east-1")
	vertical := &testVertical{
		ID:         uuid.Must(uuid.NewV4()).String(),
		RefreshKey: uuid.Must(uuid.NewV4()).String(),
		Source:     uuid.Must(uuid.NewV4()).String(),
	}

	if err := verticalsTable.PutItem(&Input{Item: vertical}); err != nil {
		t.Fatal(err)
	}

	time.Sleep(1 * time.Second)
	params := map[string]interface{}{"ID": vertical.ID}
	err := verticalsTable.DeleteItem(&Input{Key: params})
	assert.Nil(t, err)
}

package dynamodb

import (
	"strconv"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

// convertToAttributeValue will convert any value to a dynamodb.AttributeValue
// it is usefull to allow users to send values as map[string]interface{} in their
// queries, without having to remember the dynamodb.AttributeValue interface
func convertToAttributeValue(v interface{}) *dynamodb.AttributeValue {
	var value *dynamodb.AttributeValue
	switch v.(type) {
	case []byte:
		value = &dynamodb.AttributeValue{B: v.([]byte)}
	case [][]byte:
		value = &dynamodb.AttributeValue{BS: v.([][]byte)}
	case int:
		value = &dynamodb.AttributeValue{N: aws.String(strconv.Itoa(v.(int)))}
	case []int:
		var converted []*string
		for _, v := range v.([]int) {
			converted = append(converted, aws.String(strconv.Itoa(v)))
		}
		value = &dynamodb.AttributeValue{NS: converted}
	case string:
		value = &dynamodb.AttributeValue{S: aws.String(v.(string))}
	case []string:
		var converted []*string
		for _, v := range v.([]string) {
			converted = append(converted, aws.String(v))
		}
		value = &dynamodb.AttributeValue{SS: converted}
	case bool:
		value = &dynamodb.AttributeValue{BOOL: aws.Bool(v.(bool))}
	}

	return value
}

// searchToDynamo converts a simple map[string]string to a DynamoDB scan or query input values.
// It returns a map[string]*string to be used as the ExpressionAttributeNames from the dynamodb inputs,
// a map[string]*dynamodb.AttributeValue to be used as the ExpressionAttributeValues from the dynamodb inputs
// and a string to be used as the FilterExpression from dynamodb inputs
func searchToDynamo(params map[string]interface{}, filter string, prefix ...string) (map[string]*string, map[string]*dynamodb.AttributeValue, string) {
	prefixNames := ""
	prefixValues := ""

	if prefix != nil {
		if len(prefix) > 0 {
			prefixNames = prefix[0]
		}
		if len(prefix) > 1 {
			prefixValues = prefix[1]
		}
	}

	attributesNames := make(map[string]*string)
	attributesValues := make(map[string]*dynamodb.AttributeValue)
	filterExpression := make([]string, 0)

	for k, v := range params {
		value := convertToAttributeValue(v)
		nameField := prefixNames + k
		valueField := prefixValues + k

		attributesValues[valueField] = value
		attributesNames[nameField] = aws.String(k)

		filterExpression = append(filterExpression, nameField+"="+valueField)
	}

	if filter == "" {
		filter = strings.Join(filterExpression, " AND ")
	}

	return attributesNames, attributesValues, filter
}

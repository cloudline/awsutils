package dynamodb

import (
	"context"
	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-xray-sdk-go/xray"
)

func dynamoClient(region string) *dynamodb.DynamoDB {
	xray.Configure(xray.Config{LogLevel: "trace"})
	sess, _ := session.NewSession(&aws.Config{
		Region: aws.String(region)},
	)

	// Create DynamoDB client
	dynamo := dynamodb.New(sess)
	xray.AWS(dynamo.Client)
	return dynamo
}

// DynamoTable structure for all operations
type DynamoTable struct {
	tableName string
	ctx       context.Context
	client    *dynamodb.DynamoDB
}

// NewTable creates a new instance of DynamoTable with the table name indentifier
func NewTable(ctx context.Context, tableName, region string) *DynamoTable {
	return &DynamoTable{
		tableName: tableName,
		ctx:       ctx,
		client:    dynamoClient(region),
	}
}

// GetItem finds a returns a single item from a dynamodb table.
// It fills the `dest` parameters with the element found in the database table.
func (t *DynamoTable) GetItem(filter *Input, dest interface{}) error {
	if len(filter.Key) < 1 {
		return fmt.Errorf("*Input.Key is required for getting items")
	}
	_, values, _ := searchToDynamo(filter.Key, filter.Filter)

	// create get operation information
	input := &dynamodb.GetItemInput{
		TableName: aws.String(t.tableName),
		Key:       values,
	}

	// execute GetItem operation
	result, err := t.client.GetItemWithContext(t.ctx, input)
	if err != nil {
		return err
	}

	return dynamodbattribute.UnmarshalMap(result.Item, &dest)
}

// MustScan returns a result of a search ignoring the errors
// TODO: should log fatal the errors
func (t *DynamoTable) MustScan(filter *Input) *ItemList {
	list, _ := t.Scan(filter)
	return list
}

// Scan searches in a dynamo table scanning the whole table
func (t *DynamoTable) Scan(filter *Input) (*ItemList, error) {
	names, values, expression := searchToDynamo(filter.Params, filter.Filter, "#", ":")
	input := &dynamodb.ScanInput{
		ExpressionAttributeNames:  names,
		ExpressionAttributeValues: values,
		FilterExpression:          aws.String(expression),
		TableName:                 aws.String(t.tableName),
	}

	list := NewItemList()

	// loop through dynamo data pages
	for {
		// Make the DynamoDB Query API call
		result, err := t.client.ScanWithContext(t.ctx, input)
		if err != nil {
			return nil, fmt.Errorf("Got unexpected error scanning table %s: %s", t.tableName, err.Error())
		}

		// append items to the list
		list.items = append(list.items, result.Items...)

		// check to see if there are more pages, if not exit the loop
		if result.LastEvaluatedKey == nil {
			break
		}

		// there were more data pages, so set the starting point and query again
		input.SetExclusiveStartKey(result.LastEvaluatedKey)
	}

	return list, nil
}

// MustQuery executes a Query but it ignores the error
func (t *DynamoTable) MustQuery(indexName string, filter *Input) *ItemList {
	list, _ := t.Query(indexName, filter)
	return list
}

// Query gets filtered data from a DynamoDB table.
// It returns a slice of elements and an error if any
func (t *DynamoTable) Query(indexName string, filter *Input) (*ItemList, error) {
	names, values, expression := searchToDynamo(filter.Params, filter.Filter, "#", ":")

	input := &dynamodb.QueryInput{
		TableName:                 aws.String(t.tableName),
		IndexName:                 aws.String(indexName),
		ExpressionAttributeNames:  names,
		ExpressionAttributeValues: values,
		KeyConditionExpression:    aws.String(expression),
	}

	if filter.ExclusiveStartKey != nil {
		input.ExclusiveStartKey = filter.ExclusiveStartKey
	}

	list := NewItemList()

	// loop through dynamo data pages
	for {
		// Make the DynamoDB Query API call
		result, err := t.client.QueryWithContext(t.ctx, input)
		if err != nil {
			return nil, fmt.Errorf("Got unexpected error querying table %s: %s", t.tableName, err.Error())
		}

		// append items to the list
		list.items = append(list.items, result.Items...)

		// check to see if there are more pages, if not exit the loop
		if result.LastEvaluatedKey == nil {
			break
		}

		// there were more data pages, so set the starting point and query again
		input.SetExclusiveStartKey(result.LastEvaluatedKey)
	}

	// all is well, return the items
	return list, nil
}

// PutItem puts an item in the database. This operation can be used for create
// or update items.
func (t *DynamoTable) PutItem(filter *Input) error {
	i, err := dynamodbattribute.MarshalMap(filter.Item)
	if err != nil {
		return err
	}

	putInput := &dynamodb.PutItemInput{
		Item:      i,
		TableName: aws.String(t.tableName),
	}

	if filter.Filter != "" {
		putInput.ConditionExpression = aws.String(filter.Filter)
	}

	// execute Put operation
	if _, err = t.client.PutItemWithContext(t.ctx, putInput); err != nil {
		isDuplicate := strings.Contains(err.Error(), "ConditionalCheckFailedException")
		if isDuplicate {
			return fmt.Errorf("Attempt to create item failed because %s", err)
		}
		return fmt.Errorf("Got unexpected error calling put item: %s", err.Error())
	}

	return nil
}

// UpdateItem updates an element from the database.
// The update method will use the Input.Key to know which element has to replace and
// the Input.Params to set the new values.
func (t *DynamoTable) UpdateItem(input *Input) error {
	names, values, expression := searchToDynamo(input.Params, input.Filter, "#", ":")
	_, key, _ := searchToDynamo(input.Key, "")

	// We replace the default 'AND' values for commas if the user didn't supplied any
	// custom filter for this query
	if input.Filter == "" {
		expression = "set " + strings.Replace(expression, " AND ", ", ", -1)
	}

	update := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: values,
		ExpressionAttributeNames:  names,
		TableName:                 aws.String(t.tableName),
		Key:                       key,
		ReturnValues:              aws.String("UPDATED_NEW"),
		UpdateExpression:          aws.String(expression),
	}

	if input.ReturnValue != "" {
		update.ReturnValues = aws.String(input.ReturnValue)
	}

	if _, err := t.client.UpdateItemWithContext(t.ctx, update); err != nil {
		return err
	}

	return nil
}

// DeleteItem will remove an element from the database.
// It requires the Input.Key in order to create the deletion key.
func (t *DynamoTable) DeleteItem(filter *Input) error {
	if len(filter.Key) < 1 {
		return fmt.Errorf("*Input.Key is required for deleting items")
	}

	_, key, _ := searchToDynamo(filter.Key, filter.Filter)

	deleteInput := &dynamodb.DeleteItemInput{
		TableName: aws.String(t.tableName),
		Key:       key,
	}

	if _, err := t.client.DeleteItemWithContext(t.ctx, deleteInput); err != nil {
		return fmt.Errorf("Delete Subscription failed: %s" + err.Error())
	}

	return nil
}

// ItemList is a wrapper over []map[string]*dynamodb.AttributeValue that adds a
// UnmarshalList method, to convert Dynamodb Items into any kind of struct slice
type ItemList struct {
	items []map[string]*dynamodb.AttributeValue
}

// NewItemList creates a new ItemList with a initialized slice on the items field
func NewItemList() *ItemList {
	return &ItemList{items: make([]map[string]*dynamodb.AttributeValue, 0)}
}

// Items returns the list of dynamo items stored in the list
func (l *ItemList) Items() []map[string]*dynamodb.AttributeValue {
	return l.items
}

// UnmarshalList populates the dest with the data in the items field
func (l *ItemList) UnmarshalList(dest interface{}) error {
	return dynamodbattribute.UnmarshalListOfMaps(l.items, dest)
}

// Input represents a groups of *Search params and an Filter.
// Filter has been added in case of needing an special expression
type Input struct {
	Filter            string
	Params            map[string]interface{}
	Item              interface{}
	ReturnValue       string
	Key               map[string]interface{}
	ExclusiveStartKey map[string]*dynamodb.AttributeValue
}

module bitbucket.org/cloudline/awsutils/dynamodb

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3 // indirect
	github.com/aws/aws-sdk-go v1.19.1
	github.com/aws/aws-xray-sdk-go v0.9.4
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.3.0
	golang.org/x/net v0.0.0-20190324223953-e3b2ff56ed87 // indirect
)

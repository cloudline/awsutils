package logging

import (
	log "github.com/sirupsen/logrus"
	"os"
)

var functionName string

func init() {
	// fetch the host lambda name to include it in the log entry
	functionName = os.Getenv("AWS_LAMBDA_FUNCTION_NAME")
	if functionName == "" {
		functionName = "unknown"
	}
}

// NewLog returns a log entity
func NewLog() *log.Entry {

	fields := log.WithFields(log.Fields{
		"entity": functionName,
	})

	log.SetFormatter(&log.JSONFormatter{})
	return fields
}

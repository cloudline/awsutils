package logging

import (
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLogging(t *testing.T) {

	t.Run("log", func(t *testing.T) {

		// Build up a log message based on various pieces of information
		NewLog().Trace("this is ", "the number ", 1, "error: ", errors.New("nil pointer"))

	})

}

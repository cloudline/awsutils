package http

import (
	"encoding/json"
	"errors"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCustomHTTPError(t *testing.T) {
	err := NewHTTPError("This is a custom error", http.StatusBadGateway)
	response := NewResponse().Error(err)

	assert.Equal(t, http.StatusBadGateway, response.StatusCode)
}

func TestBadRequest(t *testing.T) {
	response := NewResponse().BadRequest(errors.New("400 Bad Request"))
	assert.Equal(t, http.StatusBadRequest, response.StatusCode)
}

func TestOK(t *testing.T) {
	body := map[string]string{"hello": "world!"}
	response := NewResponse().OK(body)
	jsonStr, err := json.Marshal(body)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusOK, response.StatusCode)
	assert.Equal(t, string(jsonStr), response.Body)
}

func TestHeaders(t *testing.T) {
	headers := map[string]string{"Authorization": "12345678901234567890"}
	response := NewResponse().SetHeaders(headers).NoContent()

	assert.Equal(t, http.StatusNoContent, response.StatusCode)
	assert.Empty(t, response.Body)
	assert.Equal(t, "12345678901234567890", response.Headers["Authorization"])
}

func TestRemoveHeader(t *testing.T) {
	headers := map[string]string{"Authorization": "12345678901234567890"}
	response := NewResponse().SetHeaders(headers).RemoveHeader("Authorization").NoContent()

	assert.Equal(t, http.StatusNoContent, response.StatusCode)
	assert.Empty(t, response.Headers["Authorization"])
}

package http

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
)

// Response is a wrapper arround events.APIGatewayProxyResponse
type Response struct {
	*events.APIGatewayProxyResponse
}

// setError well encapsulate the error into a json string with the message and the code.
// will print the error to the console too.
// TODO: instead of printing the error to the console use a logging system
func (r *Response) setError(err *HTTPError) events.APIGatewayProxyResponse {
	r.StatusCode = err.Code()
	r.Body = fmt.Sprintf(`{ "errorMessage": "%s", "errorCode": %d }`, err.Error(), err.Code())
	fmt.Println("ERROR: " + err.Error())
	return *r.APIGatewayProxyResponse
}

// setSuccess creates an events.APIGatewayProxyResponse with cache if enabled and the correct content-type
// Content-Type now only makes a distinction between plain/text and encoding/json. If there is no body the
// response will delete the Content-Type.
func (r *Response) setSuccess(status int, body interface{}) events.APIGatewayProxyResponse {
	if body != nil {
		responseBody, _ := json.Marshal(body)
		r.Body = string(responseBody)
	}

	r.StatusCode = status
	return *r.APIGatewayProxyResponse
}

// SetHeaders adds multiple HTTP headers to the response
func (r *Response) SetHeaders(header map[string]string) *Response {
	for k, v := range header {
		r.Headers[k] = v
	}
	return r
}

// RemoveHeader removes an HTTP header from the response
func (r *Response) RemoveHeader(header string) *Response {
	delete(r.Headers, header)
	return r
}

// RemoveHeaders removes multiple HTTP headers
func (r *Response) RemoveHeaders(headers []string) *Response {
	for _, h := range headers {
		r.RemoveHeader(h)
	}
	return r
}

// OK returns 200 response
func (r *Response) OK(body interface{}) events.APIGatewayProxyResponse {
	return r.setSuccess(http.StatusOK, body)
}

// Created returns a 201 response
func (r *Response) Created(body interface{}) events.APIGatewayProxyResponse {
	return r.setSuccess(http.StatusCreated, body)
}

// NoContent returns a 204 with no body
func (r *Response) NoContent() events.APIGatewayProxyResponse {
	return r.setSuccess(http.StatusNoContent, nil)
}

// BadRequest returns a 400 response
func (r *Response) BadRequest(err error) events.APIGatewayProxyResponse {
	return r.setError(convertToHTTPError(err, http.StatusBadRequest))
}

// NotFound returns a 404 response
func (r *Response) NotFound(err error) events.APIGatewayProxyResponse {
	return r.setError(convertToHTTPError(err, http.StatusNotFound))
}

// Forbidden returns a 403 response
func (r *Response) Forbidden(err error) events.APIGatewayProxyResponse {
	return r.setError(convertToHTTPError(err, http.StatusForbidden))
}

// Conflict returns a 409 response
func (r *Response) Conflict(err error) events.APIGatewayProxyResponse {
	return r.setError(convertToHTTPError(err, http.StatusConflict))
}

// InternalServerError returns a 500 response
func (r *Response) InternalServerError(err error) events.APIGatewayProxyResponse {
	return r.setError(convertToHTTPError(err, http.StatusInternalServerError))
}

// Error is a way to return any HTTPError you might need in case a helper method is not implemented
func (r *Response) Error(err *HTTPError) events.APIGatewayProxyResponse {
	return r.setError(err)
}

// NewResponse initializes a new events.APIGatewayProxyResponse with reponse headers
func NewResponse() *Response {
	headers := map[string]string{
		"Access-Control-Allow-Origin":      "*",
		"Access-Control-Allow-Credentials": "true",
	}

	return &Response{&events.APIGatewayProxyResponse{Headers: headers}}
}

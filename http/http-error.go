package http

// HTTPError it's a custom type of error that can handle a status code (ideally an HTTP StatusCode)
type HTTPError struct {
	message    string
	statusCode int
}

// Code returns the current http status code
func (e *HTTPError) Code() int {
	return e.statusCode
}

// Error implements the error interfacea
func (e *HTTPError) Error() string {
	return e.message
}

// NewHTTPError creates and initializes a new HTTPError
func NewHTTPError(message string, code int) *HTTPError {
	return &HTTPError{message, code}
}

// convertToHTTPError converts a simple error into an *HTTPError
func convertToHTTPError(err error, code int) *HTTPError {
	if httpError, ok := err.(*HTTPError); ok {
		httpError.statusCode = code
		return httpError
	}

	return NewHTTPError(err.Error(), code)
}

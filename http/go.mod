module bitbucket.org/cloudline/awsutils/http

require (
	github.com/aws/aws-lambda-go v1.8.1
	github.com/aws/aws-sdk-go v1.16.21
	github.com/stretchr/testify v1.3.0
)

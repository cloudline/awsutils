# HTTP Package

## Response
Response is a wrapper around `events.APIGatewayProxyResponse` that simplifies the interaction with APIGateway responses.

### Example of usage
```go
import (
	chttp "bitbucket.org/cloudline/awsutils/http"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func handler(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
    res:= chttp.NewResponse()

    data, err := DynamoDB().GetItem()
    if err != nil {
        return res.InternalServerError(err), nil
    }

    if data == nil {
        return res.NotFound(errors.New("Entity not found")), nil
    }

    return res.OK(data), nil
}

func main() {
    lambda.Start(handler)
}
```
Error response will print the error in the console so it can be catched by Cloudwatch.

> **TODO:** need to implement a proper logging system so the users can log error responses in their own way 
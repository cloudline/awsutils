# Logging Package

## Logging
This package implements the pattern prescribed in the Cloudline Solutions development patterns and practices Confluence page.
Please read the Confluence document to understand the log types and flow process.

### Example of usage
```go
import (
	
	"github.com/pkg/errors"
	logs chttp "bitbucket.org/cloudline/awsutils/logging"
)


func main() {
	
    /* log messages can be built up through the invariant signature, which will take
   	and nicely format pretty much any interface{}
    */
   	
   	err := errors.New("an error")
   	logs.NewLog().Error("message part 1"," message part 2",3,err)
   	logs.NewLog().Warning("message part 1"," message part 2",3,err)
   	logs.NewLog().Info("message part 1"," message part 2",3,err)
   	logs.NewLog().Trace("message part 1"," message part 2",3,err)
}
```


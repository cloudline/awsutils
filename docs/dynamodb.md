# DynamoDB package

## Creating a table
This libary works with the concept of tables. Tables structs are a representation of DynamoDB table.

To create a table instance just call the `NewTable` method as follows:
```go
import (
	"context"
	
	dynamo "bitbucket.org/cloudline/awsutils/dynamodb"
	"github.com/aws/aws-lambda-go/lambda"
)

func hanlder(ctx context.Context) {
	table := dynamo.NewTable(ctx, "table-name", "aws-region")	
	// other operations
}
func main() {
	lambda.Start(handler)
}
```
> NOTE: replace `table-name` and `aws-region` with your own information.

## Get a single item
This method will retrieve a single item from a table that matches the `Key` defined in the `Input` struct.
Lets see an example:
```go
dest := &MyValues{}
input := &dynamo.Input{Key: map[string]interface{}{"ID": "nJMYKemsVTaVfBnWYoiqC97AVhDG4MHj9REfQZyn"}}
err := table.GetItem(input, &dest)
```
> NOTE: For the primary key, you must provide all of the attributes.
For example, with a simple primary key, you only need to provide a value for the partition key. 
For a composite primary key, you must provide values for both the partition key and the sort key.

## Scan items
You can scan a table when your are not querying a table index. Try to avoid this method as much as possible.
```go
input := &Input{
    Params: map[string]interface{}{"some-key": "some-value"},
}

list, err := table.Scan(input)
if err != nil {
	return err
}

dest := make([]*MyEvent, len(list.items))
err := list.UnmarshalList(&dest)
```

The library has a helper method that will fail silently and can make the code cleaner. Remember that if Scan fails it won't return
any error so the following `UnmarshalList` will fail as the return value from `MustScan` will be `nil`.
```go
input := &Input{
    Params: map[string]interface{}{"some-key": "some-value"},
}

// act
dest := make([]*MyEvent, 0)
if err := table.MustScan(input).UnmarshalList(&dest); err != nil {
    message = err.Error()
}
```

## Query Items
This is the best way for finding items in a DynamoDB table as it uses indexes to filter the results. 
```go
input := &Input{
	Filter: "#eventType = :eventType AND #eventTime >= :eventTime",
	Params: map[string]interface{}{
		"eventType": "some-event-type",
		"eventTime": "2019-03-06T14:00:00Z",
	},
}
list, err := table.Query("my-custom-events", input)
if err != nil {
	return err
}

dest := make([]*MyEvent, len(list.items))
err := list.UnmarshalList(&dest)
```
In this example we use the optional `Filter` field for the input, because the library will create an equal comparison
between fields and values. As in this case we want to get the events that has an `eventTime` greater than the given value,
we need to give the input our own custom filter.
The query operation also has a `MustQuery` and will work exactly as the `MustScan` method works.

## Put Item
In order to create new items in the database we have to use the `PutItem` method. Lets see how it works
```go
event := &MyEvent{ID: "some-unique-id", Name: "Dinner with Jim", EventType: "dinner", EventTime: "2019-03-06T14:00:00Z"}
input := &Input{Item: evt, Filter: "attribute_not_exists(id)"}
err := table.PutItem(input)
```
In this case, the `Filter` is also optional and it is used to prevent duplicated items.

## Update Item
Update item needs two fields, the `Key` to idenfity the elements to be updated and the `Params` that are the fields 
we are going to update.
For example:
```go
input := &Input{
	Params: map[string]interface{}{"EventType": "business dinner"},
	Key: map[string]interface{}{"ID": "some-unique-id"},
}
err = verticalsTable.UpdateItem(input)
```

## Delete Item
Delete item has the same signature as `GetItem` but in this case will remove the element from the database.
```go
input := &Input{Key: map[string]interface{}{"ID": "some-unique-id"}}
err := table.DeleteItem(input)
```
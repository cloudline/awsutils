# Auth0 Package

## About
This package is designed primarily for integrating Auth0 with a AWS Lambda Authorizer. 

### Example Lambda Authorizer Using Auth0 package
```go
import (
	"context"
	"errors"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	auth0 "bitbucket.org/cloudline/awsutils/auth0"

)

// Main entry point used by Lambda Authorizer
func handler(ctx context.Context, event events.APIGatewayCustomAuthorizerRequest) (events.APIGatewayCustomAuthorizerResponse, error){

	// get the bearer token from the authorization header and attempt validation
	jwt, err := auth0.ParseAndValidate(event.AuthorizationToken,"https://yowl-dev.auth0.com/.well-known/jwks.json")
	if err != nil{
		fmt.Println("failed to validate token: ",err.Error())
		return events.APIGatewayCustomAuthorizerResponse{}, errors.New("Unauthorized")
	}

	// if there was no error, the jwt is fine.

	// check to make sure there is a "sub" key in the claims. This is our user id in most cases
	if jwt.Claims["sub"] == nil{
		fmt.Println("token was validated but there was no 'sub' value")
		return events.APIGatewayCustomAuthorizerResponse{}, errors.New("Unauthorized")
	}

	// extract the user id and create a policy document to return to the endpoint Lambda
	principal := jwt.Claims["sub"].(string)
	return generatePolicy(principal,"allow"),nil

}
func generatePolicy(principalId, effect string) events.APIGatewayCustomAuthorizerResponse {

	authResponse := events.APIGatewayCustomAuthorizerResponse{PrincipalID: principalId}

	authResponse.PolicyDocument = events.APIGatewayCustomAuthorizerPolicy{
		Version: "2012-10-17",
		Statement: []events.IAMPolicyStatement{
			{
				Action:   []string{"execute-api:Invoke"},
				Effect:   effect,
				Resource: []string{"*"}, // we allow access to all endpoints because this lambda is shared and cached
			},
		},
	}

	/* It is possible to return a custom object with information from the jwt. It looks like this:
	user := make(map[string]interface{})
	user["user"] = `{ "name": "tom", "admin": true}`
	authResponse.Context = user
	*/
	return authResponse
}



func main() {
	lambda.Start(handler)
}


```
### Example API Gateway Endpoint Lambda
Here we see a simple example of how to get the user id from an API Gateway endpoint where a Lambda authorizer handled authorization.

```go
import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type Authorizer struct{
	IntegrationLatency	int `json:"integrationLatency"`
	PrincipalID string `json:"principalId"`
}

func handler(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error){

	// convert the Authorizer map to a JSON byte array
	auth,_ := json.Marshal(request.RequestContext.Authorizer)
	
	// convert the JSON string to a struct so it is more useful
	authorizer := Authorizer{}
	_ = json.Unmarshal(auth, &authorizer)
	fmt.Println(authorizer.PrincipalID)

	// ... do whatever your endpoint does here and return the result...
}
func main() {
	lambda.Start(handler)
}

```